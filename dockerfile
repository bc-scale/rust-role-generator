FROM rust:latest

WORKDIR /usr/src/rust-role-generator
COPY . .

RUN cargo install --path .

CMD ["rust-role-generator"]
