<h1 align="center">Rust-Role-Generator<project-name></h1>

<p align="center">This project provides a docker image/file to run a telegram bot. This bot can be used to create game lobbys that can send roles to registered players anonymously (e.g. for MTG). Feel free to add more game modes and role sets that can be used.<project-description></p>

## Links

- [Repo](https://gitlab.com/devbauerflorian/rust-role-generator> "<project-name> Repo")

- [Bugs](https://gitlab.com/devbauerflorian/rust-role-generator/issues "Issues Page")

## Built With

- cargo
- teloxide
- tokio

## CI

- Docker images for ARM platform can be built using the CI pipeline (running on Raspberry PI 4s)

## Future Updates

- [ ] Select role sets
- [ ] Improve bot messages

## Author

**Bauer Florian**

- [Profile](https://gitlab.com/bauerflorian "Bauer Florian")
- [Email](mailto:mail@bauerflorian.de?subject=Rust-Role-Generator "Rust-Role-Generator")
- [Website](https://bauerflorian.de "Welcome")

## 🤝 Support

Contributions, issues, and feature requests are welcome!

Give a ⭐️ if you like this project!
