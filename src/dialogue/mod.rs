mod states;

use crate::dialogue::states::{
    ActiveState, RegisteredState, StartState,
};
use derive_more::From;
use teloxide::macros::Transition;

#[derive(Transition, Clone, From)]
pub enum Dialogue {
    Start(StartState),
    Registered(RegisteredState),
    Active(ActiveState),
}

impl Default for Dialogue {
    fn default() -> Self {
        Self::Start(StartState)
    }
}